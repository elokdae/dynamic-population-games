# Dynamic Population Games (DPGs)

## Description
This software package computes Stationary Nash Equilibria (SNE) in Dynamic Population Games (DPGs). DPGs are useful to model competitive scenarios involving a large number of players, who have individual state dynamics and optimize long-term payoffs.
Examples of real-world problems that can be modelled as a DPG include how people would behave under an epidemic threat, how people would spend currency in a self-contained economy, when it would be best to charge your EV, and many others.
Learn more about DPGs in the publication:

Elokda, Ezzat, Andrea Censi, and Saverio Bolognani. "Dynamic Population Games: A Tractable Intersection of Mean Field Games and Population Games." _IEEE Control Systems Letters_ (2024, submitted).

## Installation
Project dependencies are included `requirements.txt`

## Usage
To compute a SNE for your desired DPG, follow the following steps:
1. Specify the parameters of your setting (e.g., number of types, states, actions, etc.), as well as the parameters for SNE computation (e.g., step sizes, evolutionary dynamic, etc.), in `config.py`. Multiple configurations could be specified to test different parameter combinations.
2. Specify the immediate payoff function in `payoff.py`, which takes as input the mean field consisting of the current type-state distribution `distribution_prob_type_state` and policy `policy_given_type_state_prob_action`, and outputs `payoff_given_type_state_action`, a `num_types` x `num_states` x `num_actions` sized tensor of the payoff for each type-state-action tuple.
3. Specify the state transition function in `state_transition.py`, which similarly takes as input the mean field `distribution_prob_type_state` and `policy_given_type_state_prob_action`, and outputs `transition_given_type_state_action_prob_nextstate`, a `num_types` x `num_states` x `num_actions` x `num_states` sized tensor of the state transition probability for each type-state-action-next state tuple.
4. Run `main.py`. The SNE is computed for each parameter configuration in `config.py`. The computation progress is displayed on the terminal.
5. Once computation is done, figures of the SNE policy and stationary state distribution are stored in `out` folder.

Notes about implementation:
- The implementation assumes that all type-states have the same number of actions. The easiest way to disallow certain actions for some types/states is to assign them a very low payoff in `payoff_given_type_state_action.py`.
- The implementation relies on the `numpy.einsum` to efficiently operate on high-dimensional tensors. For this reason, long and explicit variable names are used to keep track of what the different dimensions represent. For example, `policy_given_type_state_prob_action` is a tensor in which dimension 0 corresponds to type, 1 corresponds to state, and 2 corresponds action.

## Authors
Ezzat Elokda, Andrea Censi, Saverio Bolognani

## License
Refer to LICENSE.txt for license information
