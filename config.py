import numpy as np
from typing import List
from copy import copy


## Config class
class Config:
    def __init__(self,
                 num_types: int,
                 num_states: int,
                 num_actions: int,
                 distribution_prob_type: np.array,
                 discount_factor_given_type: List[float],
                 initial_distribution_given_type_prob_state: np.array = None,
                 initial_policy_given_type_state_prob_action: np.array = None,
                 evolutionary_dynamics_step_size: float = 0.1,
                 evolutionary_dynamic_given_type: List[str] = None,
                 best_response_perturbation_given_type: List[float] = None,
                 policy_rate_given_type: List[float] = None,
                 distribution_tolerance: float = 1e-6,
                 policy_tolerance: float = 1e-6,
                 max_iterations: int = 1000,
                 value_tolerance: float = 1e-6,
                 value_max_iterations: int = 10000,
                 type_names: List[str] = None,
                 state_names: List[str] = None,
                 action_names: List[str] = None,
                 ):
        ## Setting parameters
        self.num_types = num_types
        self.num_states = num_states
        self.num_actions = num_actions
        self.distribution_prob_type = distribution_prob_type
        self.discount_factor_given_type = discount_factor_given_type

        ## Mean field initialization
        if initial_distribution_given_type_prob_state is None:
            # Default: uniform distribution
            self.initial_distribution_given_type_prob_state = np.array([[1.0 / num_states] * num_states] * num_types)
        else:
            self.initial_distribution_given_type_prob_state = initial_distribution_given_type_prob_state
        if initial_policy_given_type_state_prob_action is None:
            # Default: uniform random policy
            self.initial_policy_given_type_state_prob_action = np.array(
                [[[1.0 / num_actions] * num_actions] * num_states] * num_types)
        else:
            self.initial_policy_given_type_state_prob_action = initial_policy_given_type_state_prob_action

        ## Evolutionary dynamics parameters
        self.evolutionary_dynamics_step_size = evolutionary_dynamics_step_size  # for discretization of continuous dynamics
        if evolutionary_dynamic_given_type is None:
            # Default: perturbed best response dynamics
            self.evolutionary_dynamic_given_type = ['perturbed-best-response'] * num_types
        else:
            self.evolutionary_dynamic_given_type = evolutionary_dynamic_given_type
        if best_response_perturbation_given_type is None:  # smoothing for perturbed best response dynamic. Higher value leads to less smoothing
            # Default: 1000.0
            self.best_response_perturbation_given_type = [1000.0] * num_types
        else:
            self.best_response_perturbation_given_type = best_response_perturbation_given_type
        if policy_rate_given_type is None:
            # Default: 0.1
            self.policy_rate_given_type = [0.1] * num_types
        else:
            self.policy_rate_given_type = policy_rate_given_type

        ## Termination parameters
        self.distribution_tolerance = distribution_tolerance
        self.policy_tolerance = policy_tolerance
        self.max_iterations = max_iterations
        self.value_tolerance = value_tolerance
        self.value_max_iterations = value_max_iterations

        ## Optional: give types/states/actions meaningful names for visualization purposes
        if type_names is None:
            self.type_names = list(range(num_types))
        else:
            self.type_names = type_names
        if state_names is None:
            self.state_names = list(range(num_states))
        else:
            self.state_names = state_names
        if action_names is None:
            self.action_names = list(range(num_actions))
        else:
            self.action_names = action_names


## Dictionary of configurations to compute SNE for
# Example: two epidemic settings with shortsighted vs. farsighted agents
configs = {
    'epidemic-shortsighted': Config(num_types=1,  # 1 type only
                                    num_states=4,  # (S)usceptible, (A)symptomatically Infected, (I)nfected, (R)ecovered
                                    state_names=['S', 'A', 'I', 'R'],
                                    num_actions=7,  # how many other agents to interact with (from 0 to 6)
                                    distribution_prob_type=np.array([1.0]),
                                    discount_factor_given_type=[0.5],  # shortsighted agents
                                    # Few initial infections
                                    initial_distribution_given_type_prob_state=np.array([0.9, 0.05, 0.05, 0.0]),
                                    evolutionary_dynamics_step_size=1.0,
                                    policy_rate_given_type=[0.001],
                                    max_iterations=10000
                                    )
}
configs['epidemic-farsighted'] = copy(configs['epidemic-shortsighted'])  # inherent same parameters
configs['epidemic-farsighted'].discount_factor_given_type = [0.99]  # change relevant parameter - farsighted agents
