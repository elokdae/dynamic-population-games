import numpy as np

import config as cf


## Evolutionary dynamic function for policy update
# Implements discrete time approximation of evolutionary dynamic for policy evolution
def evolutionary_dynamic(policy_given_type_state_prob_action: np.array, value_given_type_state_action: np.array,
                         config: cf.Config) -> np.array:
    next_policy_given_type_state_prob_action = np.zeros((config.num_types, config.num_states, config.num_actions))

    for type in range(config.num_types):
        if config.evolutionary_dynamic_given_type[type] == 'replicator':
            pass  # TODO
        elif config.evolutionary_dynamic_given_type[type] == 'best-response':
            pass  # TODO
        elif config.evolutionary_dynamic_given_type[type] == 'smith':
            pass  # TODO
        elif config.evolutionary_dynamic_given_type[type] == 'projection':
            pass  # TODO
        else:
            # Default: perturbed best response
            next_policy_given_type_state_prob_action[type, :, :] = perturbed_best_response_dynamic(
                policy_given_type_state_prob_action[type, :, :], value_given_type_state_action[type, :, :],
                config.best_response_perturbation_given_type[type],
                config.evolutionary_dynamics_step_size * config.policy_rate_given_type[type])

    return next_policy_given_type_state_prob_action


## Perturbed best response dynamic
def perturbed_best_response_dynamic(policy_given_state_prob_action: np.array, value_given_state_action: np.array,
                                    perturbation: float, step_size: float) -> np.array:
    perturbed_value_given_state_action = perturbation * value_given_state_action
    next_policy_given_state_prob_action = np.exp(
        perturbed_value_given_state_action - np.max(perturbed_value_given_state_action, axis=1)[:, None])
    next_policy_given_state_prob_action = next_policy_given_state_prob_action / np.sum(
        next_policy_given_state_prob_action, axis=1)[:, None]
    next_policy_given_state_prob_action = ((1 - step_size) * policy_given_state_prob_action
                                           + step_size * next_policy_given_state_prob_action)

    return next_policy_given_state_prob_action
