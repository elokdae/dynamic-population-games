import numpy as np
from pathlib import Path

import config as cf
import plot
from payoff import payoff_function
from state_transition import state_transition_function
from evolutionary_dynamics import evolutionary_dynamic

if __name__ == '__main__':
    for config_name, config in cf.configs.items():
        print('_' * 20)
        print(f'Config: {config_name}')
        print('_' * 20)

        ## Initialization
        iteration = 0
        distribution_prob_type_state = np.einsum('t,s->ts', config.distribution_prob_type,
                                                 config.initial_distribution_given_type_prob_state)
        policy_given_type_state_prob_action = np.copy(config.initial_policy_given_type_state_prob_action)
        value_given_type_state = np.zeros((config.num_types, config.num_states))
        distribution_error = np.inf
        policy_error = np.inf

        ## Plot initial mean field
        config_dir = f'out/{config_name}/'
        type_state_distributions_dir = config_dir + 'type-state-distributions/'
        policies_dir = config_dir + 'policies/'
        Path(type_state_distributions_dir).mkdir(parents=True, exist_ok=True)
        Path(policies_dir).mkdir(parents=True, exist_ok=True)
        plot.plot_type_state_distribution(distribution_prob_type_state, config, type_state_distributions_dir + 'init-')
        plot.plot_policy(policy_given_type_state_prob_action, config, policies_dir + 'init-')

        ## Main SNE computation loop
        while ((distribution_error > config.distribution_tolerance or policy_error > config.policy_tolerance)
               and iteration < config.max_iterations):
            # Display status
            print(
                f'Iteration {iteration}: distribution error {distribution_error:.6f}, policy error {policy_error:.6f}')

            # Get payoffs and state transitions for current mean field
            payoff_given_type_state_action = payoff_function(distribution_prob_type_state,
                                                             policy_given_type_state_prob_action, config)
            transition_given_type_state_action_prob_nextstate = state_transition_function(distribution_prob_type_state,
                                                                                          policy_given_type_state_prob_action,
                                                                                          config)

            # Expected reward and state transition matrix
            payoff_given_type_state = np.einsum('tsa,tsa->ts', policy_given_type_state_prob_action,
                                                payoff_given_type_state_action)
            transition_given_type_state_prob_nextstate = np.einsum('tsa,tsan->tsn',
                                                                   policy_given_type_state_prob_action,
                                                                   transition_given_type_state_action_prob_nextstate)

            # Compute value function for current mean field using value iteration
            value_iteration = 0
            value_error = np.inf
            while value_error > config.value_tolerance and value_iteration < config.value_max_iterations:
                next_value_given_type_state = payoff_given_type_state + np.einsum('t,tsn,tn->ts',
                                                                                  config.discount_factor_given_type,
                                                                                  transition_given_type_state_prob_nextstate,
                                                                                  value_given_type_state)
                value_error = np.max(abs(next_value_given_type_state - value_given_type_state))
                value_given_type_state = np.copy(next_value_given_type_state)
                value_iteration += 1

            # State-action value function
            value_given_type_state_action = payoff_given_type_state_action + np.einsum('t,tsan,tn->tsa',
                                                                                       config.discount_factor_given_type,
                                                                                       transition_given_type_state_action_prob_nextstate,
                                                                                       value_given_type_state)

            ## State distribution update
            # Following is a discrete time approximation of projection dynamic for the state distribution
            next_distribution_prob_type_state = np.einsum('ts,tsn->tn', distribution_prob_type_state,
                                                          transition_given_type_state_prob_nextstate)
            next_distribution_prob_type_state = (
                    (1 - config.evolutionary_dynamics_step_size) * distribution_prob_type_state
                    + config.evolutionary_dynamics_step_size * next_distribution_prob_type_state)
            next_distribution_prob_type_state /= np.sum(
                next_distribution_prob_type_state)  # corrects for any numerical drift

            ## Policy update
            next_policy_given_type_state_prob_action = evolutionary_dynamic(policy_given_type_state_prob_action,
                                                                            value_given_type_state_action, config)

            ## Finish up iteration
            distribution_error = np.max(abs(next_distribution_prob_type_state - distribution_prob_type_state))
            policy_error = np.max(abs(next_policy_given_type_state_prob_action - policy_given_type_state_prob_action))

            distribution_prob_type_state = np.copy(next_distribution_prob_type_state)
            policy_given_type_state_prob_action = np.copy(next_policy_given_type_state_prob_action)

            iteration += 1

        ## Plot SNE results
        plot.plot_type_state_distribution(distribution_prob_type_state, config, type_state_distributions_dir + 'sne-')
        plot.plot_policy(policy_given_type_state_prob_action, config, policies_dir + 'sne-')