import numpy as np

import config as cf


## Payoff function -- TO BE IMPLEMENTED
# This code is an example from epidemic modelling
def payoff_function(distribution_prob_type_state: np.array, policy_given_type_state_prob_action: np.array,
                    config: cf.Config) -> np.array:
    payoff_given_type_state_action = np.zeros((config.num_types, config.num_states, config.num_actions))

    lockdown_action = 3  # agents are allowed to interact with up to 3 others without penalty
    lockdown_cost = 10.0
    illness_cost = 40.0

    state_S = config.state_names.index('S')
    state_A = config.state_names.index('A')
    state_I = config.state_names.index('I')
    state_R = config.state_names.index('R')

    # Agents get a higher payoff the more they interact with others
    payoff_given_type_state_action[:, :, 0:lockdown_action + 1] = range(lockdown_action + 1)

    # But if they exceed a lockdown threshold, they incur a high cost
    payoff_given_type_state_action[:, :, lockdown_action + 1:] = -lockdown_cost

    # If they are infected with symptoms, they incur a high cost representing severity of illness, and are obligated to stay home
    payoff_given_type_state_action[:, state_I, 0] = -illness_cost
    payoff_given_type_state_action[:, state_I, 1:] = -illness_cost - lockdown_cost

    return payoff_given_type_state_action
