import numpy as np
import matplotlib
import matplotlib.pyplot as plt

import config as cf

DEFAULT_FIGURE_SIZE = (6.4, 4.8)
WHITE_TO_RED_COLOR_DICT = {'red': [(0.0, 1.0, 1.0),
                                   (1.0, 1.0, 1.0)],

                           'green': [(0.0, 1.0, 1.0),
                                     (1.0, 0.0, 0.0)],

                           'blue': [(0.0, 1.0, 1.0),
                                    (1.0, 0.0, 0.0)]}
white_to_red_color_map = matplotlib.colors.LinearSegmentedColormap('WhiteToRed', segmentdata=WHITE_TO_RED_COLOR_DICT)


def plot_type_state_distribution(distribution_prob_type_state: np.array, config: cf.Config, file_prefix: str) -> None:
    figure, axes = plt.subplots(1, config.num_types,
                                figsize=(DEFAULT_FIGURE_SIZE[0] * config.num_types, DEFAULT_FIGURE_SIZE[1]))

    if config.num_types == 1:
        axes.bar(config.state_names, distribution_prob_type_state[0, :])
        axes.set_title(f'type {config.type_names[0]}')
        axes.set_xlabel('state')
        axes.set_ylabel('distribution')
    else:
        for type in range(config.num_types):
            axes[type].bar(config.state_names, distribution_prob_type_state[type, :])
            axes[type].set_title(f'type {config.type_names[type]}')
            axes[type].set_xlabel('state')
            if type == 0:
                axes[type].set_ylabel('distribution')

    plt.savefig(file_prefix + 'type-state-distribution.svg', bbox_inches='tight')
    plt.close()

    return None


def plot_policy(policy_given_type_state_prob_action: np.array, config: cf.Config, file_prefix: str) -> None:
    figure, axes = plt.subplots(1, config.num_types,
                                figsize=(DEFAULT_FIGURE_SIZE[0] * config.num_types, DEFAULT_FIGURE_SIZE[1]))

    if config.num_types == 1:
        axes.imshow(np.transpose(policy_given_type_state_prob_action[0, :, :]), cmap=white_to_red_color_map,
                    origin='lower', vmin=0.0, vmax=1.0)
        axes.set_title(f'type {config.type_names[0]}')
        axes.set_xlabel('state')
        axes.set_ylabel('action')
        axes.set_xticklabels([0] + config.state_names)
        axes.set_yticklabels([0] + config.action_names)
    else:
        for type in range(config.num_types):
            axes[type].imshow(np.transpose(policy_given_type_state_prob_action[type, :, :]),
                              cmap=white_to_red_color_map, origin='lower', vmin=0.0, vmax=1.0)
            axes[type].set_title(f'type {config.type_names[type]}')
            axes[type].set_xlabel('state')
            if type == 0:
                axes[type].set_ylabel('action')
            axes.set_xticklabels([0] + config.state_names)
            axes.set_yticklabels([0] + config.action_names)

    plt.savefig(file_prefix + 'policy.svg', bbox_inches='tight')
    plt.close()

    return None
