import numpy as np

import config as cf


## State transition function -- TO BE IMPLEMENTED
# This code is an example from epidemic modelling
def state_transition_function(distribution_prob_type_state: np.array, policy_given_type_state_prob_action: np.array,
                              config: cf.Config) -> np.array:
    transition_given_type_state_action_prob_nextstate = np.zeros(
        (config.num_types, config.num_states, config.num_actions, config.num_states))

    prob_A_given_S = 0.2  # probability of getting infected upon interacting with an infected agent
    prob_I_given_A = 0.08  # probability of asymptomatically infected agent to develop symptoms
    prob_R_given_A = 0.08  # probability of asymptomatically infected agent to recover and develop immunity
    prob_R_given_I = 0.04  # probability of infected agent to recover and develop immunity
    prob_S_given_R = 0.01  # probability of recovered agent to lose immunity

    state_S = config.state_names.index('S')
    state_A = config.state_names.index('A')
    state_I = config.state_names.index('I')
    state_R = config.state_names.index('R')

    # A transitions to I or R, or remains in A
    transition_given_type_state_action_prob_nextstate[:, state_A, :, state_I] = prob_I_given_A
    transition_given_type_state_action_prob_nextstate[:, state_A, :, state_R] = prob_R_given_A
    transition_given_type_state_action_prob_nextstate[:, state_A, :, state_A] = 1.0 - prob_I_given_A - prob_R_given_A

    # I transitions to R, or remains in I
    transition_given_type_state_action_prob_nextstate[:, state_I, :, state_R] = prob_R_given_I
    transition_given_type_state_action_prob_nextstate[:, state_I, :, state_I] = 1.0 - prob_R_given_I

    # R transitions to S, or remains in R
    transition_given_type_state_action_prob_nextstate[:, state_R, :, state_S] = prob_S_given_R
    transition_given_type_state_action_prob_nextstate[:, state_R, :, state_R] = 1.0 - prob_S_given_R

    # For S, probability of getting infected depends on proportion of infected to non-infected agents that are actively interacting
    mass_active_given_state = np.einsum('ts,tsa,a->s', distribution_prob_type_state,
                                        policy_given_type_state_prob_action, range(config.num_actions))
    mass_active_AI = mass_active_given_state[state_A] + mass_active_given_state[state_I]
    mass_active_total = np.sum(mass_active_given_state)
    prob_AI_encounter = mass_active_AI / (mass_active_total + 1e-2)     # 1e-2 ensures no loss of continuity
    prob_infection_upon_interaction = prob_AI_encounter * prob_A_given_S

    # If no interactions, agent is safe
    transition_given_type_state_action_prob_nextstate[:, state_S, 0, state_S] = 1.0

    # The more interactions, the higher the probability of getting infected
    for action in range(1, config.num_actions):
        prob_no_infection = pow(1 - prob_infection_upon_interaction, action)
        transition_given_type_state_action_prob_nextstate[:, state_S, action, state_S] = prob_no_infection
        transition_given_type_state_action_prob_nextstate[:, state_S, action, state_A] = 1 - prob_no_infection

    return transition_given_type_state_action_prob_nextstate
